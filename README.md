# Algae Direct Air Carbon Capture DACC

Conceptual project - to date no part of this project has been built!

## Introduction

Below I outline an approached to algae DAC that can be done at home and iterated on in an open source way. To be clear this can’t capture much carbon at this stage, it’s just a way to start and test out a proof of concept idea.

The machine is built around acrylic plastic tubes because they are highly transparent compared to other plastics (this lets more light through). The bace of the machine is made of a single 3d print, housing all the in and out lets for the system.
The tube size chosen in this model is 140mm outer diameter with a 5mm wall thickness at a hight of 50cm.

This system is intended for people who want to experiment with growing algae. To try out growing different strains, using different feed stocks, and putting in different lighting conditions.

## How it works
The clear cylinder placed somewhere with a good light source provides the environment for the algae to grow. The aerator takes air from the room and feeds it into the bottom of the cylinder, some of the C02 from that air is converted by the algae to biomass. Once a week the cylinder is drained back into the holding tank. It’s drained through a kitchen strainer that catches most of the algae. The strainer is then hung outside on a washing line or outside of a window to dry. Once dry the user is left with a small piece of dried condensed biomass containing some captured carbon.
(This will not be an impactful amount of carbon, this small V0 version is just for experimentation).

This project is being built under the: GNU AFFERO GENERAL PUBLIC LICENSE Version 3, 19 November 2007
