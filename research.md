
Why Algae?
It’s safe to work with and grow at home making it suitable for an open hardware project.

Hackaday search for similar projects:
https://hackaday.io/project/170760-project-kilogram
https://hackaday.io/project/11129-home-brew-photobioreactor
https://hackaday.io/project/159232-phytoplankton-power-hybrid-microbial-fuel-cell
https://hackaday.io/project/21621-autonomous-liquid-growing-algae-environment
https://hackaday.io/project/22022-algae-experiment-for-fuel-and-materials
https://hackaday.io/project/12398-aeroponic-all-year-greenhouse-on-a-budged 
Actions: DM all of these projects and look for collaboration.

Existing products: https://www.fastcompany.com/90365320/this-kit-makes-it-shockingly-easy-and-pretty-to-farm-your-own-algae-at-home
Algae powered building: https://www.fastcompany.com/90182374/this-entire-building-is-powered-by-its-algae-filled-walls

Advertised future: https://www.research.uky.edu/news/algae-co2-capture-part-1-how-it-works Power station Algae capture

https://hypergiant.tv/hypergiant-eos-autonomous-carbon-sequestration-algae-bioreactor 
Hyper Giant said in 2018 they would open source and sell a DAC Algae machine for home, office and commercial use: https://www.forbes.com/sites/cognitiveworld/2020/01/23/hypergiant-ai-algae-climate-changehttps://www.fastcompany.com/90404556/this-personal-carbon-sequestration-device-uses-algae-to-capture-co2

Studies suggest two pounds of CO2 on average is utilised per each pound of algae grown. http://biomassmagazine.com/articles/6962/co2-and-algae-projectsCO2 and Algae Projects @BiomassMagazineAn opportunity to sequester carbon and foster algae productionThe company claims that its Eos Bioreactor is 400x more effective at capturing carbon than trees. https://www.inceptivemind.com/algae-based-eos-bioreactor-absorbs-co2-acre-trees/9186/

"Using Algae for Bioenergy Carbon Capture and Storage (ABECCS) takes up a small land and water footprint while yielding similar or better results to other technologies. In a controlled study, it proved more energy efficient and less expensive than direct air capture (DAC) and far more compact than BECCS with soy." https://parametric.press/issue-02/algae/

"Tiny Algae and the Political Theater of Planting One Trillion TreesTo fight climate change, it’s time to start thinking big by thinking small".

Lets open source develop the best Algae Bioreactor possible.
Hypergiant say they use: ‘chlorella vulgaris’ algae and get 400x better performance than a tree.
"The magic of bioreactors lies in the ability to hyper-regulate an environment. The bioreactor is constantly taking measurements of the algae’s growth environment, and then using machine learning algorithms to find the perfect balance of light, temperature, and pH levels that algae will flourish under. This minimizes the need for human intervention and maximizes the level of CO₂ that can be sequestered."

